# -*- coding: utf-8 -*-
# vim: ft=sls

{%- from "prometheus/map.jinja" import prometheus with context %}

prometheus-rules-dir:
  file.directory:
    - name: {{ prometheus.config_dir }}/rules
    - makedirs: True

prometheus-targetgroups-dir:
  file.directory:
    - name: {{ prometheus.config_dir }}/tgroups
    - makedirs: True

prometheus-config:
  file.managed:
    - name: {{ prometheus.config_dir }}/prometheus.yml
    - source: salt://prometheus/files/prometheus.yml.j2
    - mode: 644
    - user: root
    - group: root
    - template: jinja
    {%- if prometheus.service != False %}
    - watch_in:
       - service: prometheus-service
       - cmd: prometheus-validate-config
    {%- endif %}

{% for name, rules in salt['pillar.get']('prometheus:rules', {}).iteritems() %}
prometheus-rule-{{ name }}:
  file.managed:
    - name: {{ prometheus.config_dir }}/rules/{{ name }}.yml
    - source: salt://prometheus/files/rules.yml.j2
    - mode: 644
    - user: root
    - group: root
    - template: jinja
    - context:
        prom_rules: {{ rules }}
    - require:
       - file: prometheus-rules-dir
    {%- if prometheus.service != False %}
    - watch_in:
       - service: prometheus-service
       - cmd: prometheus-validate-config
    {%- endif %}
{% endfor %}

prometheus-validate-config:
  cmd.run:
    - name: >-
        {{ prometheus.dist_dir }}/prometheus-{{ prometheus.version }}/promtool
        check config {{ prometheus.config_dir }}/prometheus.yml

# Enabling the service here to ensure each state is independent.
prometheus-service:
  service.running:
    - name: prometheus
    # Reload service if config changes
    - reload: True
    - enable: {{ prometheus.service }}
    - require:
      - cmd: prometheus-validate-config
