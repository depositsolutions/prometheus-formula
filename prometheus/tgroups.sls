# -*- coding: utf-8 -*-
# vim: ft=sls

{%- from "prometheus/map.jinja" import prometheus with context %}

prometheus-tgroups-dir:
  file.directory:
    - name: {{ prometheus.config_dir }}/tgroups
    - makedirs: True

{% for name, tgroup in salt['pillar.get']('prometheus:tgroups', {}).iteritems() %}
prometheus-tgroup-{{ name }}:
  file.managed:
    - name: {{ prometheus.config_dir }}/tgroups/{{ name }}.yml
    - source: salt://prometheus/files/tgroups.yml.j2
    - mode: 644
    - user: root
    - group: root
    - template: jinja
    - context:
        prom_tgroup: {{ tgroup }}
    - require:
       - file: prometheus-tgroups-dir
    - watch_in:
      - service: prometheus-tgroup-service-reload
{% endfor %}

prometheus-validate-config-tgroups:
  cmd.run:
    - name: >-
        {{ prometheus.dist_dir }}/prometheus-{{ prometheus.version }}/promtool
        check config {{ prometheus.config_dir }}/prometheus.yml

# Enabling the service here to ensure each state is independent.
prometheus-tgroup-service-reload:
  service.running:
    - name: prometheus
    # Reload service if config changes
    - reload: True
    - enable: {{ prometheus.service }}
    - require:
      - cmd: prometheus-validate-config-tgroups
