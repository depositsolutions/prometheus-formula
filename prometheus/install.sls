# -*- coding: utf-8 -*-
# vim: ft=sls

{% from "prometheus/map.jinja" import prometheus with context %}

prometheus-create-user:
  user.present:
    - name: {{ prometheus.service_user }}
    - shell: /bin/false
    - system: True
    - home: /dev/null
    - createhome: False

prometheus-create-group:
  group.present:
    - name: {{ prometheus.service_group }}
    - members:
      - {{ prometheus.service_user }}
    - require:
      - user: {{ prometheus.service_user }}

prometheus-bin-dir:
  file.directory:
   - name: {{ prometheus.bin_dir }}
   - makedirs: True

prometheus-dist-dir:
 file.directory:
   - name: {{ prometheus.dist_dir }}
   - makedirs: True

prometheus-config-dir:
  file.directory:
    - name: {{ prometheus.config_dir }}
    - makedirs: True

prometheus-default-config:
  file.managed:
    - name: {{ prometheus.config_dir }}/prometheus.yml
    - source: salt://prometheus/files/default_prometheus.yml
    - unless:
      - '[[ -f {{ prometheus.config_dir }}//prometheus.yml ]]'

prometheus-data-dir:
  file.directory:
    - name: {{ prometheus.data_dir }}
    - makedirs: True
    - user: {{ prometheus.service_user }}
    - group: {{ prometheus.service_group }}

prometheus-install-binary:
  archive.extracted:
    - name: {{ prometheus.dist_dir }}/prometheus-{{ prometheus.version }}
    - source: https://github.com/prometheus/prometheus/releases/download/v{{ prometheus.version }}/prometheus-{{ prometheus.version }}.{{ grains['kernel'] | lower }}-{{ prometheus.arch }}.tar.gz
    - source_hash: https://github.com/prometheus/prometheus/releases/download/v{{ prometheus.version }}/sha256sums.txt
    - options: --strip-components=1
    - user: root
    - group: root
    - enforce_toplevel: False
    - unless:
      - '[[ -f {{ prometheus.dist_dir }}/prometheus-{{ prometheus.version }}/prometheus ]]'
  file.symlink:
    - name: {{ prometheus.bin_dir }}/prometheus
    - target: {{ prometheus.dist_dir }}/prometheus-{{ prometheus.version }}/prometheus
    - mode: 0755
    - unless:
      - '[[ -f {{ prometheus.bin_dir }}/prometheus ]] && {{ prometheus.bin_dir }}/prometheus -v | grep {{ prometheus.version }}'

prometheus-install-service:
  file.managed:
    - name: /etc/systemd/system/prometheus.service
    - source: salt://prometheus/files/prometheus.service.j2
    - template: jinja
  service.running:
    - name: prometheus
    - enable: {{ prometheus.service }}
