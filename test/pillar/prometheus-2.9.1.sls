prometheus:
  bin_dir: '/usr/bin'
  dist_dir: '/opt/prometheus/dist'
  config_dir: '/etc/prometheus'
  data_dir: '/var/lib/prometheus'
  log_level: debug
  version: '2.9.1'
  service: True
  build: False
  service_user: 'prometheus'
  service_group: 'prometheus'
  config:
    global:
      scrape_interval:     15s
      evaluation_interval: 15s
      external_labels:
          monitor: 'codelab-monitor'
    scrape_configs:

      - job_name: 'prometheus'
        static_configs:
          - targets: ['localhost:9090']
  rules:
{% raw %}
    production_hosts:
      groups:
      - name: rules/production_hosts.rules
        rules:
        - alert: Production server CPU load
          expr: >-
            node:load5_per_cpu:ratio{instance=~".*prod.*deposit:9100|.*(pzp|p24|pdb|pmh|pfb).*:9100"} > 1
          for: 10m
          labels:
            severity: critical
            routing_key: victorops-infra
          annotations:
            summary: High load5 on node
            description: The load5 per cpu on {{ $labels.instance }} is higher that 1 for more that 10 minutes
        - alert: server disk space is low
          expr: >-
            node:filesystem_avail:percentage{instance=~".*prod.*deposit:9100|.*(pzp|p24|pdb|pmh|pfb).*:9100", mountpoint="/"} < 10 AND ON (instance, mountpoint) node_filesystem_avail{instance=~".*prod.*deposit:9100|.*(pzp|p24|pdb|pmh|pfb).*:9100", mountpoint="/"} / (1024 * 1024) < 3000
          for: 10m
          labels:
            severity: warning
          annotations:
            description: Disk space on an {{ $labels.instance }} server is low
        - alert: server disk space is very low
          expr: >-
            node:filesystem_avail:percentage{instance=~".*prod.*deposit:9100|.*(pzp|p24|pdb|pmh|pfb).*:9100", mountpoint="/"} < 5 AND ON (instance, mountpoint) node_filesystem_avail{instance=~".*prod.*deposit:9100|.*(pzp|p24|pdb|pmh|pfb).*:9100", mountpoint="/"} / (1024 * 1024) < 1000
          for: 10m
          labels:
            severity: critical
            routing_key: victorops-infra
          annotations:
            description: Disk space on {{ $labels.instance }} server is very low
    erasmus_host:
      groups:
      - name: rules/erasmus.rules
        rules:
        - alert: An Erasmus server might run out of disk in the next 12 hours
          expr: predict_linear(node_filesystem_free{instance=~'erasmus0.*:9100', mountpoint=~'/|/data'}[1h], 12 * 3600) < 0 and ON() hour() > 7 < 18 and day_of_week() > 0 < 6
          for: 10m
          labels:
            severity: warning
            routing_key: victorops-infra
          annotations:
            description: An Erasmus server might run out of disk space in the next few hours
{% endraw %}
